/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import Router from './src/Router';
import AuthContextProvider from './src/context/AuthContext';

function App(): JSX.Element {

  return (
    <AuthContextProvider>
      <Router />
    </AuthContextProvider>
  );
}

export default App;
