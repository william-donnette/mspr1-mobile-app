import { AuthContextReturn } from '../context/AuthContext';
import { IProductMold } from '../factory/ProductFactory';
import Auth, { IAuth } from '../models/Auth';
import { staticImplements } from '../utils/functions';
import AuthApi from './AuthApi';

interface IProductApi {
  getAll: (auth: AuthContextReturn) => Promise<Array<IProductMold>>;
  get: (auth: AuthContextReturn, id: string) => Promise<IProductMold>;
}

@staticImplements<IProductApi>()
export default class ProductApi {

  static async refreshToken({auth, setAuth} : AuthContextReturn, callback : Function){
    AuthApi.refresh({auth, setAuth}).then(
      data => {
        setAuth(new Auth({
          token: data.access_token,
          authenticated: true,
          refreshToken: data.refresh_token,
        }));
        return callback({ auth, setAuth });
      }
    ).catch(() => {
      setAuth(new Auth());
    });
  }


  static async getAll({ auth, setAuth }: AuthContextReturn) {
    try {
      const response = await fetch('https://api-revendeurs.mspr1.homkizz.com/api/Products', {
        headers: {
          authorization: 'Bearer ' + auth.token,
        },
      });
      console.log(response);
      if (response.status === 401){
        return this.refreshToken({auth, setAuth}, this.getAll);
      }
      const json = await response.json();
      return json;
    } catch (e) {
      console.log(e);
    }
  }

  static async get({ auth, setAuth }: AuthContextReturn, id: string) {
    try {
      const response = await fetch(`https://api-revendeurs.mspr1.homkizz.com/api/Products/${id}`, {
        headers: {
          authorization: 'Bearer ' + auth.token,
        },
      });
      if (response.status === 401){
        return this.refreshToken({auth, setAuth}, this.get);
      }
      const json = await response.json();
      return json;
    } catch (e) {
      console.log(e);
    }
  }
}
