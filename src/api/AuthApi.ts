import { AuthContextReturn } from '../context/AuthContext';
import { IAuthConstructor } from '../models/Auth';
import { staticImplements } from '../utils/functions';
export interface IAuthApi {
    login: (username: string, password: string, totp: string) => Promise<Array<IAuthConstructor>>;
    refresh: (context: AuthContextReturn) => Promise<Array<IAuthConstructor>>;
}

@staticImplements<IAuthApi>()
export default abstract class AuthApi {
    static async login (username: string, password: string, totp: string) {
        const response = await fetch('https://keycloak.homkizz.com/realms/API1/protocol/openid-connect/token', {
            body: `client_id=webshop-api&username=${username}&password=${password}&grant_type=password&client_secret=K60R8LYtmtmJBM1CQrbuJJDgay1W2vDw&totp=${totp}`,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            method: 'POST',
          });
        const json = await response.json();
        return json;
    }

    static async refresh ({auth}: AuthContextReturn) {
        const response = await fetch('https://keycloak.homkizz.com/realms/API1/protocol/openid-connect/token', {
            body: `client_id=webshop-api&grant_type=refresh_token&client_secret=K60R8LYtmtmJBM1CQrbuJJDgay1W2vDw&refresh_token=${auth.refreshToken}`,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            method: 'POST',
          });
        const json = await response.json();
        return json;
    }
}

/* Login
 LOG  {"error": "invalid_grant", "error_description": "Invalid user credentials"}
 LOG  Login
 LOG  {"access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICItSkdOc0FTQ1pmWWRodW1mcDBpbHBaZzZyaE5FUFVMQ28zbEJ5OURIVW1rIn0.eyJleHAiOjE2NzY5OTEyOTUsImlhdCI6MTY3Njk5MDk5NSwianRpIjoiMzI5YTc5ZDYtMGZhMC00OThhLWE2N2MtNDQyYjMxNDhiMzY0IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5ob21raXp6LmNvbS9yZWFsbXMvQVBJMSIsImF1ZCI6IndlYnNob3AtYXBpIiwic3ViIjoiNGQ5NDAwM2YtYTM0Ni00MDc1LTlkMzMtYmI4NzY2MjM1MzRlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoid2Vic2hvcC1hcGkiLCJzZXNzaW9uX3N0YXRlIjoiNzVmYWI4NzctYjA3MC00NTQ0LTk1MDItYTNkYzc0ZDc2MGI4IiwiYWNyIjoiMSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJkZWZhdWx0LXJvbGVzLWFwaTEiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJzaWQiOiI3NWZhYjg3Ny1iMDcwLTQ1NDQtOTUwMi1hM2RjNzRkNzYwYjgiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicHJlZmVycmVkX3VzZXJuYW1lIjoid2Vic2hvcC1hcGkiLCJnaXZlbl9uYW1lIjoiIiwiZmFtaWx5X25hbWUiOiIifQ.HDv62HgHbffh4_CcsNBIz-BM5nPRNjrNZ8edBpzU5RnHGdFuO3Wfh-g23Kk7F_wcFzDcW0u7AIuXSg3byvul-h-1tKyrHvE0U_h8PL4BbcL1j4B_ZeOkYpZTX00tAZNd9CJLUMu4jS3vwc8AcgTW1C7hXjTi5U9JxtVIpEoAdmsUQqcrFkyOI1-V9hPaQNSEbhK9hGiyj_UU5hUEcx3Pv5KcY45hO0KoQ4ypb16pNyDpUfTl0Bne0RsJnlkvWOfMdcpzvEcnWVnWAYdkZiT6rkL6WMDAAIH_hhPmblkXN28Vfc5cgmUcaa7wo1rBn9Zul-3pQA9GoJLdFa0xI3H2-A", 
 "expires_in": 300, 
 "not-before-policy": 0, 
 "refresh_expires_in": 1800, 
 "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI1NGJmY2IzNi1jZTIxLTQzNGMtYWVkYi1hM2FjZmZhNmIzZDkifQ.eyJleHAiOjE2NzY5OTI3OTUsImlhdCI6MTY3Njk5MDk5NSwianRpIjoiYjlmMGJhNGMtNGQ2MC00YTc3LThjYTctYWVjYjMwY2E4NDY5IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5ob21raXp6LmNvbS9yZWFsbXMvQVBJMSIsImF1ZCI6Imh0dHBzOi8va2V5Y2xvYWsuaG9ta2l6ei5jb20vcmVhbG1zL0FQSTEiLCJzdWIiOiI0ZDk0MDAzZi1hMzQ2LTQwNzUtOWQzMy1iYjg3NjYyMzUzNGUiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoid2Vic2hvcC1hcGkiLCJzZXNzaW9uX3N0YXRlIjoiNzVmYWI4NzctYjA3MC00NTQ0LTk1MDItYTNkYzc0ZDc2MGI4Iiwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiNzVmYWI4NzctYjA3MC00NTQ0LTk1MDItYTNkYzc0ZDc2MGI4In0.qFjPSQ8onzbPjNT9hehWIEapZ5anCDYU1VQUI9yEhbQ", "scope": "email profile", "session_state": "75fab877-b070-4544-9502-a3dc74d760b8", "token_type": "Bearer"}*/