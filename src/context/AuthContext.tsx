import { FunctionComponent, ReactNode, createContext, useContext, useState } from 'react';
import Auth, { IAuth } from '../models/Auth';

interface AuthContextProviderProps {
    children: ReactNode;
}



export interface AuthContextReturn {
    auth: IAuth;
    setAuth: (auth: IAuth) => void;
}


const AuthContext = createContext<AuthContextReturn | null>(null);

export const useAuthContext = () => {
    const auth = useContext(AuthContext);
    if (!auth) {
        throw new Error('You can\'t use useAuthContext outside of AuthContextProvider');
    }
    return auth;
};

const AuthContextProvider: FunctionComponent<AuthContextProviderProps> = ({children}) => {
    const [auth, setAuth] = useState<IAuth>(new Auth());

    return (
        <AuthContext.Provider value={{auth, setAuth}}>
            {children}
        </AuthContext.Provider>
    );
};

export default AuthContextProvider;
