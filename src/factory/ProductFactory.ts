import Product, { IProduct, IProductDetails } from "../models/Product";
import { staticImplements } from "../utils/functions";
import { IFactory, IMold } from "./Factory";

export interface IProductMold extends IMold {
    createdAt: string;
    details: IProductDetails;
    id: string;
    stock: number;
    name: string;
    urlAr : string
}

@staticImplements<IFactory<IProductMold, IProduct>>()
export default class ProductFactory {
    static create({createdAt, details, name, stock, id, urlAr}: IProductMold) {
        return new Product(createdAt, name, details, stock, id, urlAr);
    }
}