import { IModel } from '../models/Model';

export interface IMold {}

export interface IFactory<U extends IMold, T extends IModel> {
    create: (mold: U) => T
}