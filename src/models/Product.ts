import { IModel } from './Model';

export interface IProduct extends IModel {
    createdAt: Date,
    name: string,
    details: IProductDetails,
    stock: number,
    id: string,
    price: number,
    urlAr : string
}

export interface IProductDetails {
    price: string,
    description: string,
    color: string
}

export default class Product implements IProduct {
    createdAt: Date;
    name: string;
    details: IProductDetails;
    stock: number;
    id: string;
    urlAr: string;

    constructor (createdAt: string, name: string, details: IProductDetails, stock: number, id: string, urlAr : string) {
        this.createdAt = new Date(createdAt);
        this.name = name;
        this.details = details;
        this.stock = stock;
        this.id = id;
        this.urlAr = urlAr;
    }

    get price () {
        return Number(this.details.price);
    }
}
