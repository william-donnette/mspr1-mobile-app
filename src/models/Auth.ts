
export interface IAuth extends IAuthConstructor {
    isLogged: () => boolean;
}

export interface IAuthConstructor {
    token: string;
    authenticated: boolean;
    refreshToken: string;
}

export default class Auth implements IAuth {
    token: string;
    authenticated: boolean;
    refreshToken: string;

    constructor({token, authenticated, refreshToken}: IAuthConstructor = {token: '', authenticated: false, refreshToken: ''}) {
        this.token = token;
        this.authenticated = authenticated;
        this.refreshToken = refreshToken;
    }

    isLogged() {
        return this.authenticated;
    }
}
