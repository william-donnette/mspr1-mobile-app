import ProductApi from '../api/ProductApi';
import { AuthContextReturn } from '../context/AuthContext';
import ProductFactory, {IProductMold} from '../factory/ProductFactory';
import { staticImplements } from '../utils/functions';
import { IProduct } from './Product';

export interface IProductList {
  getAll(context: AuthContextReturn): Promise<Array<IProduct>>;
  getById(context: AuthContextReturn, id: string): Promise<IProduct | null>;
}

@staticImplements<IProductList>()
export default class ProductList {

  static async getAll(context: AuthContextReturn) {
    const products: Array<IProductMold> = await ProductApi.getAll(context);
    return products.map((product: IProductMold) => ProductFactory.create(product));
  }

  static async getById(context: AuthContextReturn, id: string) {
    const product: IProductMold = await ProductApi.get(context, id);
    return ProductFactory.create(product);
  }
}
