import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ProductList from './components/ProductList';
import VizualizationAR from './components/VizualizationAR';
import ProductDetails from './components/ProductDetails';
import {FunctionComponent} from 'react';
import Login from './components/Login';
import { useAuthContext } from './context/AuthContext';

interface RouterProps {}

export type RootStackParamList = {
  Home: undefined;
  Login: undefined;
  Detail: {id: string};
  AR: {url: string};
};

const Stack = createNativeStackNavigator<RootStackParamList>();

const Router: FunctionComponent<RouterProps> = () => {
  const {auth} = useAuthContext();


  return (
    <>
      <NavigationContainer>
        <Stack.Navigator>
          {!auth.isLogged() && <Stack.Screen name={'Login'} component={Login} />}
          {auth.isLogged() && (
            <>
              <Stack.Screen name={'Home'} component={ProductList} />
              <Stack.Screen name={'Detail'} component={ProductDetails} />
              <Stack.Screen name={'AR'} component={VizualizationAR} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default Router;
