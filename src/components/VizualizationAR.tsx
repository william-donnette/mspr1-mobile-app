import { ViroARScene, ViroARPlaneSelector, ViroAmbientLight, Viro3DObject, ViroARSceneNavigator } from '@viro-community/react-viro';
import { FunctionComponent, useState } from 'react';
import { StyleSheet } from 'react-native';

interface VizualizationARProps {}

const VizualizationAR: FunctionComponent<VizualizationARProps> = () => {
    const [plane, setPlane] = useState<any>(null);

    const scene = () => (
        <ViroARScene anchorDetectionTypes={['None', 'PlanesHorizontal']}>
        <ViroAmbientLight color="#ffffff" />
        <ViroARPlaneSelector alignment={'Horizontal'} onPlaneSelected={setPlane}>
          <Viro3DObject
            source={{uri: 'https://samuelpeybernesdev.fr/storage/CoffeeMaker.obj'}}
            resources={[require('../../assets/coffee/CoffeeMaker.mtl')]}
            position={plane ? plane.center : [0, 0, 0]}
            scale={[0.02, 0.02, 0.02]}
            rotation={[90, 0, 0]}
            type="OBJ"
            onDrag={() => {}}
          />
        </ViroARPlaneSelector>
      </ViroARScene>
    );

    return (
        <ViroARSceneNavigator
          autofocus={true}
          initialScene={{scene}}
          style={styles.f1}
        />
    );
};

const styles = StyleSheet.create({
    f1: {flex: 1},
    helloWorldTextStyle: {
      fontFamily: 'Arial',
      fontSize: 30,
      color: '#ffffff',
      textAlignVertical: 'center',
      textAlign: 'center',
    },
});

export default VizualizationAR;
