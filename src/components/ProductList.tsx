import React, {FunctionComponent, useEffect, useState} from 'react';
import {IProduct} from '../models/Product';
import ProductRepo from '../models/ProductList';
import {StackScreenProps} from '@react-navigation/stack';
import {Button, FlatList, View} from 'react-native';
import { RootStackParamList } from '../Router';
import { useAuthContext } from '../context/AuthContext';
import Auth from '../models/Auth';

export interface ProductListProps
  extends StackScreenProps<RootStackParamList, 'Home'> {}

const ProductList: FunctionComponent<ProductListProps> = ({navigation}) => {
  const [products, setProducts] = useState<Array<IProduct>>([]);
  const context = useAuthContext();

  useEffect(() => {
    ProductRepo.getAll(context).then(data => {
      setProducts(data);
    });
  }, [context]);

  return (
    <FlatList
      data={products}
      renderItem={({item}) => (
        <View>
          <Button
            onPress={() => {
              console.log(item.name);
              navigation.navigate('Detail', {id: item.id});
            }}
            title={item.name}
          />
        </View>
      )}
      keyExtractor={item => item.id}
      scrollEnabled={true}
    />
  );
};

export default ProductList;
