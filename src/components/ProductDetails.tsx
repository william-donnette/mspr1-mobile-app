import { FunctionComponent, useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { IProduct } from '../models/Product';
import ProductList from '../models/ProductList';
import { Button, StyleSheet, Text, View } from 'react-native';
import { RootStackParamList } from '../Router';
import { useAuthContext } from '../context/AuthContext';

interface ProductDetailsProps extends StackScreenProps<RootStackParamList, 'Detail'> {}

const ProductDetails: FunctionComponent<ProductDetailsProps> = ({navigation, route}) => {
    const [product, setProduct] = useState<IProduct | null>(null);
    const context = useAuthContext();

    useEffect(() => {
        ProductList.getById(context, route.params.id)
        .then(setProduct);
    }, [context, route.params.id]);

    return (
        <View style={styles.container}>
            <Text style={styles.black}>{product?.name}</Text>
            <Text style={styles.black}>Stock: {product?.stock}</Text>
            <View>
                <Text style={styles.black}>Details:</Text>
                <Text style={styles.black}>Prix: {product?.price}€</Text>
                <Text style={styles.black}>Couleur: {product?.details.color}</Text>
                <Text style={styles.black}>Description: {product?.details.description}</Text>
            </View>
            <View style={styles.buttons}>
                <Button onPress={() => navigation.goBack()} title={'Retour'} />
                {true && <Button onPress={() => navigation.navigate('AR', {url : product?.urlAr ?? ''})} title={'Voir l\'objet en réalité augmentée'}/>}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    black: {
        color: 'black',
    },
    buttons: {
        height: '25%',
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    lastButton: {
        marginTop: 10,
    },
});

export default ProductDetails;
