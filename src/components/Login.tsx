import React, { FunctionComponent, useState } from 'react';
import { View, Text, Button, StyleSheet, TextInput } from 'react-native';
import { useAuthContext } from '../context/AuthContext';
import AuthApi from '../api/AuthApi';
import Auth from '../models/Auth';

interface LoginProps {}

const Login: FunctionComponent<LoginProps> = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [totp, setTOTP] = useState('');
  const [error, setError] = useState(false);
  const {setAuth} = useAuthContext();

  const login = async () => {
    setError(false);
      const reponse = await AuthApi.login(username, password, totp);
      if (!reponse.error) {
        setAuth(new Auth({
          token: reponse.access_token,
          authenticated: true,
          refreshToken: reponse.refresh_token,
        }));
      }
      else {
        setError(true);
      }
  };

  return (
    <View style={styles.container}>
      {error && <Text>Invalid Credential</Text>}
      <TextInput style={styles.input} placeholder="Username" value={username} onChangeText={setUsername}/>
      <TextInput style={styles.input} secureTextEntry={true} placeholder="Password" value={password} onChangeText={setPassword}/>
      <TextInput style={styles.input} placeholder="TOTP" value={totp} onChangeText={setTOTP}/>
      <Button onPress={login} title="Login" />
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'orange',
    },
    input: {
        height: 40,
        width: 200,
        marginVertical: 10,
        borderBottomWidth: 1,
        color : 'black',
    }
  });

export default Login;
