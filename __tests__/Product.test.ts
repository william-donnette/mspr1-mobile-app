import ProductApi from '../src/api/ProductApi';
import Product, { IProduct } from '../src/models/Product';
import fetchMock from 'jest-fetch-mock';
import Auth from '../src/models/Auth';
it('Le prix du produit est un nombre', () => {
    const p : IProduct = new Product('', '', {price: '5.00', description: '', color: ''}, 1, '', '');
    expect(p.price).toBe(5);
    expect(typeof p.price).toBe('number');
});

fetchMock.enableMocks();

beforeEach(() => {
    fetchMock.resetMocks();
});

it('Get product list', async () => {
    fetchMock.mockResponseOnce(JSON.stringify([
        {
            'createdAt': '2023-02-19T13:42:19.01Z',
            'name': 'Rex Bailey',
            'stock': 12059,
            'details': {
              'price': 659,
              'description': 'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',
              'color': 'red',
            },
            'id': 1,
            'urlAr': '',
          },
          {
            'createdAt': '2023-02-20T12:57:08.025Z',
            'name': 'Israel Dooley',
            'stock': 49835,
            'details': {
              'price': 663,
              'description': 'Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',
              'color': 'white',
            },
            'id': 2,
            'urlAr': '',
          },
    ]));
    const auth = new Auth();
    const setAuth = () => {};
    const productList  = await ProductApi.getAll({auth, setAuth});
    expect(productList.length).toEqual(2);
    expect(productList[0].name).toEqual('Rex Bailey');

});

it('Error get product list', async () => {
    fetchMock.mockReject(() => Promise.reject('API is down'));
    const auth = new Auth();
    const setAuth = () => {};
    const productList  = await ProductApi.getAll({auth, setAuth});
    expect(productList).toEqual(undefined);
});

it('Get product by id', async () => {
    fetchMock.mockResponseOnce(JSON.stringify(
        {
            'createdAt': '2023-02-19T13:42:19.01Z',
            'name': 'Rex Bailey',
            'stock': 12059,
            'details': {
              'price': 659,
              'description': 'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',
              'color': 'red',
            },
            'id': 1,
            'urlAr': '',
          },
    ));
    const auth = new Auth();
    const setAuth = () => {};
    const product = await ProductApi.get({auth, setAuth}, '1');
    expect(product.name).toEqual('Rex Bailey');
    expect(product.id).toEqual(1);
});

it('Error get product by wrong id', async () => {
    fetchMock.mockResponseOnce(JSON.stringify({}));
    const auth = new Auth();
    const setAuth = () => {};
    const product = await ProductApi.get({auth, setAuth}, '1000');
    expect(product.name).toEqual(undefined);
});
