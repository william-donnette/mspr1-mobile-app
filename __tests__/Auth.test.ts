import Auth, { IAuth } from "../src/models/Auth";

it('L\'authentification ne doit pas etre faite', () => {
    const a: IAuth = new Auth();
    expect(a.isLogged()).toBe(a.authenticated);
    expect(a.authenticated).toBe(false);
    expect(a.isLogged()).toBe(false);
});

it('L\'authentification doit etre faite', () => {
    const a: IAuth = new Auth({token: '1234', refreshToken: '1234', authenticated: true});
    expect(a.isLogged()).toBe(a.authenticated);
    expect(a.authenticated).toBe(true);
    expect(a.isLogged()).toBe(true);
});
